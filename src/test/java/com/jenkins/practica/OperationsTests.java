package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.jenkins.utils.DogsOperations;

public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg
	 */
	@Test
	public void testRandom() {
		// Instantiate DogsOperations class
		DogsOperations dogsOperationss = new DogsOperations();
		// Call getRandomDogImage operation and store the result on String
		String imageName = dogsOperationss.getRandomDogImage();
		// Assert true if the result string ends with '.jpg'
		assertTrue(imageName.endsWith(".jpg"));
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 */
	@Test
	public void breedsList() {
		// Instantiate DogsOperations
		DogsOperations dogsOperationsx = new DogsOperations();
		// Call getBreedList operation
		ArrayList<String> list = dogsOperationsx.getBreedList();
		// Assert true if the result ArrayList has size
		assertTrue(list.size() > 0);
	}
}
